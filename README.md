# React Native #

This is a blank react native app project

## How do I get set up? ##

* Clone down
* Ensure you have react native and dependencies [set up](http://facebook.github.io/react-native/docs/getting-started.html)
* Run ```npm i```

* open the .xcodeproj file in xcode (or run ```open GithubBrowser.xcodeproj``` in terminal)
* Hit the play button at the top left

###Contact ###

[luke@aqueduct.co.uk](mailto:luke@aqueduct.co.uk)